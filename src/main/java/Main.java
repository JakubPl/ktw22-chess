import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = Integer.parseInt(scanner.nextLine());

        //rysuj size+2 iksów
        System.out.println("x".repeat(size + 2));
        for (int y = 0; y < size; y++) {
            //System.out.print("*.".repeat(size));
            System.out.print("x");
            for (int x = 0; x < size; x++) {
                //tu rysujemy wiersz
                if ((x + y) % 2 == 0) {
                    System.out.print("*");
                } else {
                    System.out.print(".");
                }
            }
            System.out.println("x");
        }
        System.out.println("x".repeat(size + 2));
        //rysuj size+2 iksów


    }
}
